//
//  MLWallGenerator.swift
//  NinjaGame
//
//  Created by Bartosz Szewczyk on 05.11.2015.
//  Copyright © 2015 Bartosz Szewczyk. All rights reserved.
//

import Foundation
import SpriteKit

class MLWallGenerator : SKSpriteNode
{
    var generatorTimer: NSTimer?
    var walls = [MLWall]();
    var wallTrackers = [MLWall]();
    func startGeneratingWallEvery(seconds: NSTimeInterval)
    {
        generatorTimer = NSTimer.scheduledTimerWithTimeInterval(seconds, target: self, selector: "generateWall", userInfo: nil, repeats: true);
    }
    func StopGenerating()
    {
        generatorTimer?.invalidate();
    }
    
    func generateWall()
    {
        var scale:CGFloat;
        let rand = arc4random_uniform(2);
        if(rand == 0)
        {
            scale = -1.0;
        }
        else
        {
            scale = 1.0;
        }
        let wall = MLWall()
        wall.position.x = size.width/2 + wall.size.width/2;
        wall.position.y = scale * ( kMLGroundHeight/2 + wall.size.height/2);
        addChild(wall);
        walls.append(wall);
        
        wallTrackers.append(wall);
    }
    func StopWalls()
    {
        StopGenerating();
        for wall in walls{
            wall.stopMoving();
        }
    }
}
