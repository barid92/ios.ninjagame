//
//  Constans.swift
//  NinjaGame
//
//  Created by Bartosz Szewczyk on 05.11.2015.
//  Copyright © 2015 Bartosz Szewczyk. All rights reserved.
//

import Foundation
import UIKit

// config
let kMLGroundHeight:CGFloat = 20.0;
let kDefaultXToMovePerSecond: CGFloat = 320.0;


//collision detection

let heroCategory: UInt32 = 0x1 << 0;
let wallCategory: UInt32 = 0x1 << 1;

//Game variables
let kNumberOfPointsPerLevel = 5;
let kLevelGenerationsTimes: [NSTimeInterval] = [1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05];



