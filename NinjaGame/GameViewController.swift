//
//  GameViewController.swift
//  NinjaGame
//
//  Created by Bartosz Szewczyk on 03.11.2015.
//  Copyright (c) 2015 Bartosz Szewczyk. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    var sceen:GameScene!;
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        //Configurate the view
        
        let skView = view as! SKView;
        skView.multipleTouchEnabled = false;
        
        //Create and configure the scene
        
        sceen = GameScene(size: skView.bounds.size);
        sceen.scaleMode = .AspectFill;
        
        //Present the scenee
        skView.presentScene(sceen);
        
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
