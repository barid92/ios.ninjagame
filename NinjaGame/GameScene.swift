//
//  GameScene.swift
//  NinjaGame
//
//  Created by Bartosz Szewczyk on 03.11.2015.
//  Copyright (c) 2015 Bartosz Szewczyk. All rights reserved.
//

import SpriteKit

class GameScene: SKScene ,SKPhysicsContactDelegate{
    
    var movingGround:MLMovingGround!;
    var hero: MLHero!;
    var cloudGenerator:MLCloudGenerator!;
    var wallGenerator:MLWallGenerator!;
    var isStarted = false;
    var isGameOver = false;
    var currentLevel = 0;
    
    override func didMoveToView(view: SKView) {
        
        
        backgroundColor = UIColor(colorLiteralRed: 159.0/255.0, green: 201.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        addCloudGenerator();
        addMovingGround();
        addHero();
        addWallGenerator();
        addPhysicsWorld();
        addTabToStartLabel();
        addPointsLabel();
        loadHighScore();
    }
    func loadHighScore()
    {
        let defaults = NSUserDefaults.standardUserDefaults();
        let highscoreLabel = childNodeWithName("highscoreLabel") as! MLPointsLabel;
        highscoreLabel.setTo(defaults.integerForKey("highscore"));
    }
    func addPhysicsWorld()
    {
        physicsWorld.contactDelegate = self;
    }
    func addTabToStartLabel()
    {
        let tapToStartLabel = SKLabelNode(text: "Tap to start!")
        tapToStartLabel.name = "tapToStartLabel";
        tapToStartLabel.position.x = view!.center.x;
        tapToStartLabel.position.y = view!.center.y + 40;
        tapToStartLabel.fontColor = UIColor.blackColor();
        tapToStartLabel.fontName = "Helvetica";
        addChild(tapToStartLabel);
        tapToStartLabel.runAction(blinkAnimation());
    }
    func addPointsLabel()
    {
        let pointsLablel = MLPointsLabel(num: 0);
        pointsLablel.name = "pointsLabel";
        pointsLablel.position = CGPointMake(20.0, view!.frame.height - 20);
        addChild(pointsLablel);
        
        
        let hightscoreLabel = MLPointsLabel(num: 0);
        hightscoreLabel.name = "highscoreLabel";
        hightscoreLabel.position = CGPointMake(view!.frame.width - 20, view!.frame.height - 20)
        addChild(hightscoreLabel);
        
        
        
    }
    func addMovingGround()
    {
        movingGround = MLMovingGround(size: CGSizeMake(view!.frame.width, kMLGroundHeight));
        movingGround.position = CGPointMake(0, view!.frame.size.height/2);
        addChild(movingGround);
    }
    func addHero()
    {
        hero = MLHero();
        
        hero.position = CGPointMake(70,movingGround.position.y + movingGround.frame.height/2 + hero.frame.size.height/2);
        
        addChild(hero);
        hero.breathe();

    }
    func addCloudGenerator()
    {
        cloudGenerator = MLCloudGenerator(color: UIColor.clearColor(), size: view!.frame.size);
        cloudGenerator.position = view!.center;
        addChild(cloudGenerator);
        cloudGenerator.populate(7);
        cloudGenerator.startGeneratingWithSpawnTime(5);
    }
    func addWallGenerator()
    {
        wallGenerator = MLWallGenerator(color: UIColor.clearColor(), size: view!.frame.size);
        wallGenerator.position = view!.center;
        addChild(wallGenerator);
    }
    // MARK: - Game Lifecycle
    func start()
    {
        movingGround.start();
        hero.breathe();
        hero.startRunning();
        isStarted = true;
        wallGenerator.startGeneratingWallEvery(1);
        let tapToStartLabel = childNodeWithName("tapToStartLabel");
        tapToStartLabel?.removeFromParent();
        
    }
    func GameOver()
    {
        isGameOver = true;
        //stop everything
        hero.Fall();
        wallGenerator.StopWalls();
        movingGround.stop();
        hero.Stop();
        
        let tapToGameOverLabel = SKLabelNode(text: "Game Over!")
        tapToGameOverLabel.name = "tapToGameOverLabel";
        tapToGameOverLabel.position.x = view!.center.x;
        tapToGameOverLabel.position.y = view!.center.y + 40;
        tapToGameOverLabel.fontColor = UIColor.blackColor();
        tapToGameOverLabel.fontName = "Helvetica";
        addChild(tapToGameOverLabel);
        tapToGameOverLabel.runAction(blinkAnimation());
        
        //sava current points value
        let pointsLabel = childNodeWithName("pointsLabel") as! MLPointsLabel;
        let hightscoreLabel = childNodeWithName("highscoreLabel") as! MLPointsLabel;
        
        if(hightscoreLabel.number < pointsLabel.number)
        {
            hightscoreLabel.setTo(pointsLabel.number);
            let defaults = NSUserDefaults.standardUserDefaults();
            defaults.setInteger(hightscoreLabel.number, forKey: "highscore");
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        if(isGameOver)
        {
            restart();
        }
        else if(!isStarted)
        {
            start();
        }
        else
        {
            hero.StopBreathe();
            
            hero.Flip();
        }
    }
   
    func restart()
    {
        cloudGenerator.StopGenerating();
        let newScene = GameScene(size: view!.bounds.size);
        newScene.scaleMode = .AspectFill;
        view!.presentScene(newScene);
    }
    override func update(currentTime: CFTimeInterval)
    {
        if(wallGenerator.wallTrackers.count > 0)
        {
        let wall = wallGenerator.wallTrackers[0] as MLWall;
        
        let wallLocation = wallGenerator.convertPoint(wall.position, fromNode: self);
        if(wallLocation.x<hero.position.x)
        {
            wallGenerator.wallTrackers.removeAtIndex(0);
            
            let pointsLabel = childNodeWithName("pointsLabel") as! MLPointsLabel;
            pointsLabel.increment();
            if(pointsLabel.number % kNumberOfPointsPerLevel == 0)
            {
                currentLevel++;
                wallGenerator.StopGenerating();
                wallGenerator.startGeneratingWallEvery(kLevelGenerationsTimes[currentLevel]);
            }
        }
        }
    }
    
    //MARK: -SKPhysicsContactDelegate
    func didBeginContact(contact: SKPhysicsContact) {
        GameOver();
    }
    func blinkAnimation() -> SKAction
    {
        let duration = 0.4;
        let fadeOut = SKAction.fadeAlphaTo(0.0, duration: duration);
        let fadeIn = SKAction.fadeAlphaTo(1.0, duration: duration);
        let blink = SKAction.sequence([fadeOut,fadeIn]);
        return SKAction.repeatActionForever(blink);
    }
}
