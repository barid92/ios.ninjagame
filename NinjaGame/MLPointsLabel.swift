//
//  MLPointsLabel.swift
//  NinjaGame
//
//  Created by Bartosz Szewczyk on 12.11.2015.
//  Copyright © 2015 Bartosz Szewczyk. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
class MLPointsLabel : SKLabelNode
{
    var number = 0;
    init(num: Int)
    {
        super.init();
        
        fontColor = UIColor.blackColor();
        fontName = "Helvetica";
        fontSize = 20.0;
        
        number = num;
        text = "\(num)";
        
    }
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
  
    func increment()
    {
        number++;
        text = "\(number)";
    }
    func setTo(number: Int)
    {
        self.number = number;
        text = "\(self.number)";
    }
}