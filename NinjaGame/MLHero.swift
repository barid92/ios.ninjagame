//
//  MLHero.swift
//  NinjaGame
//
//  Created by Bartosz Szewczyk on 03.11.2015.
//  Copyright © 2015 Bartosz Szewczyk. All rights reserved.
//

import Foundation
import SpriteKit

class MLHero:SKSpriteNode 
{
    var body:SKSpriteNode!;
    var arm:SKSpriteNode!;
    var leftFood:SKSpriteNode!;
    var rightFood:SKSpriteNode!;
    var isUpssideDown = false;
    init()
    {
        let size = CGSizeMake(32, 44);
        super.init(texture: nil, color: UIColor.clearColor(), size: size);

        loadApperance();
        
        loadPhysicsBodyWithSize(size);
    }

    func loadPhysicsBodyWithSize(size:CGSize)
    {
        physicsBody = SKPhysicsBody(rectangleOfSize: size);
        physicsBody?.categoryBitMask = heroCategory;
        physicsBody?.contactTestBitMask = wallCategory;
        physicsBody?.affectedByGravity = false;
    }
    func loadApperance()
    {
        body = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(self.frame.size.width, 40));
        body.position = CGPointMake(0, 2);
        addChild(body);
        
        let skinColor = UIColor(red: 207.0/255.0, green: 193.0/255.0, blue: 168.0/255.0, alpha: 1.0);
        
        let Face = SKSpriteNode(color: skinColor, size: CGSizeMake(self.frame.size.width, 12));
        Face.position = CGPointMake(0, 6);
        body.addChild(Face);
        
        let eyeColor = UIColor.whiteColor();
        let leftEye = SKSpriteNode(color: eyeColor, size: CGSizeMake(6, 6));
        let rightEye = leftEye.copy() as! SKSpriteNode;
        
        let pupil = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(3, 3));
        
        pupil.position = CGPointMake(2, 0);
        
        leftEye.addChild(pupil);
        rightEye.addChild(pupil.copy() as! SKSpriteNode);
        
        leftEye.position = CGPointMake(-4, 0);
        Face.addChild(leftEye);
        
        rightEye.position = CGPointMake(14, 0);
        Face.addChild(rightEye);
        
        let eyeBrow = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(11, 1));
        eyeBrow.position = CGPointMake(-1, leftEye.size.height/2);
        leftEye.addChild(eyeBrow);
        rightEye.addChild(eyeBrow.copy() as! SKSpriteNode);
        
        let armColor = UIColor(red: 46/255, green: 46/255, blue: 46/255, alpha: 1.0);
        arm = SKSpriteNode(color: armColor, size: CGSizeMake(8, 14));
        arm.anchorPoint = CGPointMake(0.5, 0.9);
        arm.position = CGPointMake(-10, -7);
        body.addChild(arm);
        
        let hand = SKSpriteNode(color: skinColor, size: CGSizeMake(arm.size.width,5));
        hand.position = CGPointMake(0, -arm.size.height*0.9 + hand.size.height/2);
        arm.addChild(hand);
        
        leftFood = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(9, 4));
        leftFood.position = CGPointMake(-6, -size.height/2 + leftFood.size.height/2);
        addChild(leftFood);
        
        rightFood = leftFood.copy() as! SKSpriteNode;
        rightFood.position.x = 8;
        addChild(rightFood);
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func breathe()
    {
        let breatheOut = SKAction.moveByX(0, y: -2, duration: 1);
        let breatheIn = SKAction.moveByX(0, y: 2, duration: 1);
        let breathSeq = SKAction.sequence([breatheOut, breatheIn]);
        body.runAction(SKAction.repeatActionForever(breathSeq));
    }
    func Fall()
    {
        physicsBody?.affectedByGravity = true;
        physicsBody?.applyImpulse(CGVectorMake(-5, 30));
        
        let rotateBack = SKAction.rotateByAngle(CGFloat(M_PI)/2, duration: 0.4);
        runAction(rotateBack);
    }
    func Flip()
    {
        isUpssideDown = !isUpssideDown;
        
        var scale:CGFloat!;
        if(isUpssideDown)
        {
            scale = -1.0;
        }
        else
        {
            scale = 1.0;
        }
        let translate = SKAction.moveByX(0, y: scale*(body.size.height + kMLGroundHeight + 4), duration: 0.1);
        let flip = SKAction.scaleYTo(scale, duration: 0.1);
        
        runAction(translate);
        runAction(flip);
    }
    func Stop()
    {
        
        leftFood.removeAllActions();
        rightFood.removeAllActions();
    }
    func StopBreathe()
    {
        body.removeAllActions();
    }
    func startRunning()
    {
        let rotateBack = SKAction.rotateByAngle(-CGFloat(M_PI)/2.0, duration: 0.1);
        arm.runAction(rotateBack);
        preformOneRunCycle();
    }
    func preformOneRunCycle()
    {
        let up = SKAction.moveByX(0, y: 2, duration: 0.05);
        let down = SKAction.moveByX(0, y: -2, duration: 0.05);
        
        leftFood.runAction(up, completion:  { () -> Void in
            self.leftFood.runAction(down);
            self.rightFood.runAction(up, completion: { () -> Void in
                self.rightFood.runAction(down, completion: { () -> Void in
                    self.preformOneRunCycle();
                })
            })
        })
        
    }
}